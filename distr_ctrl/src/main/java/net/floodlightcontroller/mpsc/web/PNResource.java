package net.floodlightcontroller.mpsc.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.mpsc.allocation.RouteBroker;
import net.floodlightcontroller.mpsc.mapping.optimization.MetapathOpt;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.VLHolder;
import net.floodlightcontroller.mpsc.utils.LogWriter;
import net.floodlightcontroller.mpsc.web.model.SwitchRule;
import net.floodlightcontroller.mpsc.web.model.VLRules;
import net.floodlightcontroller.mpsc.web.model.VirtualLink;
import net.floodlightcontroller.mpsc.xml.GeneralConfigParser;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.mpsc.MPSCConstants;
import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.discovery.model.ITopology;
import net.floodlightcontroller.mpsc.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.HostHolderImpl;
import net.floodlightcontroller.mpsc.utils.AllocatedBwEstimator;
import net.floodlightcontroller.mpsc.web.model.PhysicalLink;
import net.floodlightcontroller.mpsc.xml.TopoConfigParser;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class PNResource extends ServerResource implements IJsonVL {
    @Get("json")
    public Set<PhysicalLink> retrieve() {
        String user = getAttribute("user");
        if (user != null && user.equals("admin")) {
            IRouteFinderService routeFinder =
                    (IRouteFinderService) getContext().getAttributes().
                            get(IRouteFinderService.class.getCanonicalName());

            ITopology t = routeFinder.getTopo();
            t.updateTopoCache();

            //collect info about all known intra SDN links
            Set<PhysicalLink> pls = convertKnownLinksToPLs(t.getKnownLinks());

            //then collect info about all known inter SDN links
            pls.addAll(convertKnownLinksToPLs(t.getKnownForeignLinks()));

            return pls;
        } else return Collections.EMPTY_SET;
    }

    private Set<PhysicalLink> convertKnownLinksToPLs(Map<Pair<INode, INode>, ILink> links) {
        Set<PhysicalLink> pls = new HashSet<>();

        for (Map.Entry<Pair<INode, INode>, ILink> e : links.entrySet()) {
            INode srcNode = e.getKey().getLeft();
            String src = srcNode.getID().toString();
            INode.INodeType srcType = srcNode.getType();

            INode dstNode = e.getKey().getRight();
            String dst = dstNode.getID().toString();
            INode.INodeType dstType = dstNode.getType();

            Pair<String, String> linkKey = new ImmutablePair<>(src, dst);
            Double Capacity = TopoConfigParser.getInstance().getLinkCapacity(linkKey);
            double capacity = Capacity == null ? MPSCConstants.MAX_HOST_LINK_BW : Capacity;
            double avBw = e.getValue().getBw();
            double latency = e.getValue().getLatency();

            pls.add(new PhysicalLink(src, srcType, srcNode.getCapacity(), srcNode.getStress(),
                    dst, dstType, dstNode.getCapacity(), dstNode.getStress(),
                    avBw, capacity, latency));
        }

        return pls;
    }
}
