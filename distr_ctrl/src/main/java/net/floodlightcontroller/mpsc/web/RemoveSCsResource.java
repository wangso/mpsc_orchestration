package net.floodlightcontroller.mpsc.web;


import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.HostHolderImpl;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.VLHolder;
import net.floodlightcontroller.mpsc.web.model.VirtualLink;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.Set;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class RemoveSCsResource extends ServerResource
{
    @Get
    public void retrieve()
    {
        String user = getAttribute("user");
        if (user != null && user.equals("admin"))
        {
            //remove all SC flows
            VLHolder.getInstance().removeAllVLs();

            IStaticFlowEntryPusherService flowPusher =
                    (IStaticFlowEntryPusherService) getContext().getAttributes().
                            get(IStaticFlowEntryPusherService.class.getCanonicalName());

            //delete OF flows
            flowPusher.deleteAllFlows();
            //release all compute resources
            //TODO: remove containers
            HostHolderImpl.getInstance().releaseAllHostsAllocatedResources();
        }
    }
}
