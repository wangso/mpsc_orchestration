package net.floodlightcontroller.mpsc.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.floodlightcontroller.mpsc.allocation.RouteBroker;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.VLHolder;
import net.floodlightcontroller.mpsc.web.model.SwitchRule;
import net.floodlightcontroller.mpsc.web.model.VLRules;
import net.floodlightcontroller.mpsc.web.model.VirtualLink;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class RemovePNResource extends ServerResource implements IJsonVL {
    @Post("json")
    public void removePNResources(String json) {
        IStaticFlowEntryPusherService flowPusher =
                (IStaticFlowEntryPusherService) getContext().getAttributes().
                        get(IStaticFlowEntryPusherService.class.getCanonicalName());

        IRouteFinderService routeFinder =
                (IRouteFinderService) getContext().getAttributes().
                        get(IRouteFinderService.class.getCanonicalName());

        if (RouteBroker.getInstance().needFlowPusher())
            RouteBroker.getInstance().setFlowPusherService(flowPusher);

        String user = getAttribute("user");

        if (user != null) {
            List<VLRules> vlRs = new ArrayList<>();
            try {
                System.out.println("[MPSC-Removing] The following JSON will be parsed:" + json);
                vlRs = jsonToVLRules(json);
            } catch (Exception ex) {
                //TODO: add proper loggining
                System.out.println("[MPSC-Removing] Cannot parse the following json request:" + json);
            }

            try {
                for (VLRules vlR : vlRs) {
                    VirtualLink vl = vlR.getVL();
                    System.out.println("[DEBUG-MPSC-Removing] Need to remove resources for the following VL: src IP="
                            + vl.getSrcIP() + " and dst IP=" + vl.getDstIP() + " switch rules=" + vlR.getSwitchRules()
                            + " their json=" + vlR.getSwitchRulesAsJSON());
                    //allocate pNode resources first
                    INode pSrc = routeFinder.getTopo().getNodeWithID(vl.getSrcIP());
                    if (pSrc != null) {
                        //TODO: call docker for containerized allocation of resources
                        System.out.println("[DEBUG-MPSC-Removing] The following physical node w/ IP="
                                + pSrc.getID() + " will remove the following resources: CPU=" + vl.getSrcCap());
                        pSrc.setStress((pSrc.getStress() - vl.getSrcCap()) < 0 ?
                                0 : pSrc.getStress() - vl.getSrcCap());
                    }
                    INode pDst = routeFinder.getTopo().getNodeWithID(vl.getDstIP());
                    if (pDst != null) {
                        //TODO: call docker for containerized allocation of resources
                        System.out.println("[DEBUG-MPSC-Removing] The following physical node w/ IP="
                                + pDst.getID() + " will allocate the following resources: CPU=" + vl.getDstCap());
                        pDst.setStress((pDst.getStress() - vl.getDstCap()) < 0 ?
                                0 : pDst.getStress() - vl.getDstCap());
                    }

                    List<INode<String>> partialPath = new ArrayList<>(vlR.getSwitchRules().size());
                    for (SwitchRule sr : vlR.getSwitchRules()) {
                        INode<String> src = routeFinder.getTopo().getNodeWithID(sr.getSrc());
                        INode<String> dst = routeFinder.getTopo().getNodeWithID(sr.getDst());
                        System.out.println("[DEBUG-MPSC-Removing] The following src=" + src
                                + " and dst=" + dst + " found for a switch rule=" + sr);
                        if (src != null && dst != null) {
                            if (partialPath.isEmpty()) {
                                partialPath.add(src);
                                partialPath.add(dst);
                            } else {
                                INode<String> lastOnRoute = partialPath.get(partialPath.size() - 1);
                                if (!lastOnRoute.equals(src) && !lastOnRoute.equals(dst)) {
                                    partialPath.add(src);
                                    partialPath.add(dst);
                                } else
                                    //ensure correct sequence on the path
                                    partialPath.add(lastOnRoute.equals(src) ? dst : src);
                            }
                        } else
                            throw new RuntimeException("[MPSC-Removing] Wrong switch rule: either src=" + sr.getSrc()
                                    + " or dst=" + sr.getDst() + "  or both are unknown!");
                    }
                    //remove stored VL path
                    VLHolder.getInstance().removeVL(vl);
                    System.out.println("[DEBUG-MPSC-Removing] The following partial path is going to be removed=" + partialPath);
                    routeFinder.removeRoute(partialPath, vl.getSrcIP(), vl.getDstIP(), vl.getBw());
                    VLHolder.getInstance().getUserVL(user).remove(vl);
                }
            } catch (Exception ex) {
                //TODO: add proper loggining
                System.out.println("[MPSC-Removing] Removing resources failed because of the following reason:"
                        + ex.getMessage());
            }
        }
    }

    public static String switchRuleToJson(List<SwitchRule> switchRules) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(switchRules);

        return json;
    }


    public static String vlRulesToJson(List<VLRules> vlRulesList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vlRulesList);

        return json;
    }

    public static List<VLRules> jsonToVLRules(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(List.class, VLRules.class));
    }

    public static List<SwitchRule> jsonToSwitchRules(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(List.class, SwitchRule.class));
    }

    public static String vlToJson(List<VirtualLink> vls) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vls);

        return json;
    }
}
