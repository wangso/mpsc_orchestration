package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class PhysicalLinkDeserializer extends JsonDeserializer<PhysicalLink> implements IJsonVL
{
    @Override
    public PhysicalLink deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException
    {
        String src = null;
        INode.INodeType srcType = null;
        double srcCap = -1.0;
        double srcStress = -1.0;
        String dst = null;
        INode.INodeType dstType = null;
        double dstCap = -1.0;
        double dstStress = -1.0;
        double avBw = -1.0;
        double capacity = -1.0;
        double latency = -1.0;

        if (jp.getCurrentToken() != JsonToken.START_OBJECT)
        {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT)
        {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME)
            {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals(""))
            {
                continue;
            }

            switch (n)
            {
                case SRC:
                    src = jp.getText();
                    break;
                case SRC_TYPE:
                    srcType = INode.INodeType.valueOf(jp.getText());
                    break;
                case SRC_CAP:
                    srcCap = jp.getDoubleValue();
                    break;
                case SRC_STRESS:
                    srcStress = jp.getDoubleValue();
                    break;
                case DST:
                    dst = jp.getText();
                    break;
                case DST_TYPE:
                    dstType = INode.INodeType.valueOf(jp.getText());
                    break;
                case DST_CAP:
                    dstCap = jp.getDoubleValue();
                    break;
                case DST_STRESS:
                    dstStress = jp.getDoubleValue();
                    break;
                case BW:
                    avBw = jp.getDoubleValue();
                    break;
                case CAPACITY:
                    capacity = jp.getDoubleValue();
                    break;
                case LATENCY:
                    latency = jp.getDoubleValue();
                    break;
                default:
                    break;
            }
        }

        if (src != null && srcType!=null && dst != null && dstType!=null && avBw >= 0 && capacity >=0 && latency >=0)
            return new PhysicalLink(src, srcType, srcCap, srcStress, dst, dstType, dstCap, dstStress,
                    avBw, capacity, latency);
        else throw new IOException("wrong PL parameters");
    }
}
