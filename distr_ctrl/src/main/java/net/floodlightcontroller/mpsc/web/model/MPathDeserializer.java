package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.web.IJsonVL;

import java.io.IOException;
import java.util.List;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class MPathDeserializer extends JsonDeserializer<MPath> implements IJsonVL
{
    @Override
    public MPath deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException
    {
        String src = null;
        String dst = null;
        List<INode<String>> path = null;
        double cost = -1.0;

        if (jp.getCurrentToken() != JsonToken.START_OBJECT)
        {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT)
        {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME)
            {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals(""))
            {
                continue;
            }

            switch (n)
            {
                case SRC:
                    src = jp.getText();
                    break;
                case DST:
                    dst = jp.getText();
                    break;
                case PATH:
                    try {
                        ObjectMapper mapper = new ObjectMapper();
                        path = mapper.readValue(jp.getText(),
                                mapper.getTypeFactory().constructCollectionType(List.class, INode.class));
                    } catch (IOException ex) {
                        System.out.println("[ERROR-MPSC] Cannot parse mpath route=" + jp.getText()
                                + "; reason:" + ex.getMessage());
                    }
                    break;
                case COST:
                    cost = jp.getDoubleValue();
                    break;
                default:
                    break;
            }
        }

        if (src != null && dst != null && path!=null && cost >=0)
            return new MPath(src, dst, path, cost);
        else throw new IOException("wrong MP parameters");
    }
}
