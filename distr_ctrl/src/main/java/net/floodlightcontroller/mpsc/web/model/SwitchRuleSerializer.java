package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.floodlightcontroller.mpsc.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class SwitchRuleSerializer extends JsonSerializer<SwitchRule> implements IJsonVL
{
    @Override
    public void serialize(SwitchRule rule, JsonGenerator jgen, SerializerProvider arg) throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeStringField(SRC, rule.src);
        jgen.writeStringField(DST, rule.dst);
        jgen.writeNumberField(BW, rule.bw);
        jgen.writeEndObject();
    }
}
