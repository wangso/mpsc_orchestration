package net.floodlightcontroller.mpsc.web;

import net.floodlightcontroller.restserver.RestletRoutable;
import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.routing.Router;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class MPSCWebRoutable implements RestletRoutable
{
    @Override
    public Restlet getRestlet(Context context)
    {
        Router router = new Router(context);
        router.attach("/SC/{user}", SCResource.class);
        router.attach("/Remove_all_SCs/{user}", RemoveSCsResource.class);
        router.attach("/PN/{user}", PNResource.class);
        router.attach("/Allocate_PN/{user}", AllocatePNResource.class);
        router.attach("/Remove_PN/{user}", RemovePNResource.class);
        router.attach("/VL/measure/{user}", VLMeasurementResource.class);
        return router;
    }

    @Override
    public String basePath()
    {
        return "/wm/mpsc";
    }
}
