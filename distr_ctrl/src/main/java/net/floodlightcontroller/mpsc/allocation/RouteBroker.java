package net.floodlightcontroller.mpsc.allocation;

import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.mpsc.allocation.impl.ResourceBrokerV01Impl;
import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;

import java.util.List;
import java.util.Map;

/**
 * Created by chemo_000 on 2/6/2015.
 */
//todo make it singleton
public class RouteBroker //todo add java docs and logs
{
    private static RouteBroker instance = null;
    private IResourceBroker irb;

    private RouteBroker() {
        irb = new ResourceBrokerV01Impl();//todo change to dependency injection
    }

    protected RouteBroker(IResourceBroker irb) {
        this.irb = irb;
    }

    public static RouteBroker getInstance() {
        if (instance == null)
            instance = new RouteBroker();

        return instance;
    }

    public <T> void reserveResources(List<INode<T>> route, double bw) {
        if (route.size() > 1) {
            //allocate in forward direction
            INode<T> src = route.get(0);
            INode<T> dst = route.get(route.size() - 1);

            for (int i = 0; i < route.size() - 1; i++) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i + 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);

                irb.allocateSpecifiedBandwidth(src, dst, currentNode, nextNode, link, bw);
            }

            //allocate in backward direction
            src = route.get(route.size() - 1);
            dst = route.get(0);

            for (int i = route.size() - 1; i > 0; i--) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i - 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);

                irb.allocateSpecifiedBandwidth(src, dst, currentNode, nextNode, link, bw);
            }
        }
    }

    public <T> void reserveResources(List<INode<T>> route, String srcIP, String dstIP, double bw) {
        if (route.size() > 1) {
            //allocate in forward direction
            for (int i = 0; i < route.size() - 1; i++) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i + 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);
                //skip resource allocation for foreign nodes...
                if (!currentNode.isForeign())
                    irb.allocateSpecifiedBandwidth(srcIP, dstIP, currentNode, nextNode, link, bw);
            }

            //allocate in backward direction
            for (int i = route.size() - 1; i > 0; i--) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i - 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);
                //skip resource allocation for foreign nodes...
                if (!currentNode.isForeign())
                    irb.allocateSpecifiedBandwidth(dstIP, srcIP, currentNode, nextNode, link, bw);
            }
        }
    }

    public <T> void releaseSpecifiedResources(List<INode<T>> route, String srcIP, String dstIP, double bw) {
        if (route.size() > 1) {
            //allocate in forward direction
            for (int i = 0; i < route.size() - 1; i++) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i + 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);
                //skip resource allocation for foreign nodes...
                if (!currentNode.isForeign())
                    irb.releaseSpecifiedBandwidth(srcIP, dstIP, currentNode, nextNode, link, bw);
            }

            //allocate in backward direction
            for (int i = route.size() - 1; i > 0; i--) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i - 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);
                //skip resource allocation for foreign nodes...
                if (!currentNode.isForeign())
                    irb.releaseSpecifiedBandwidth(dstIP, srcIP, currentNode, nextNode, link, bw);
            }
        }
    }

    public <T> void releaseSpecifiedResources(List<INode<T>> route, double bw) {
        if (route.size() > 1) {
            INode<T> src = route.get(0);
            INode<T> dst = route.get(route.size() - 1);

            for (int i = 0; i < route.size() - 1; i++) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i + 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);

                irb.releaseSpecifiedBandwidth(src, dst, currentNode, nextNode, link, bw);
            }

            //allocate in backward direction
            src = route.get(route.size() - 1);
            dst = route.get(0);

            for (int i = route.size() - 1; i > 0; i--) {
                INode<T> currentNode = route.get(i);
                INode<T> nextNode = route.get(i - 1);
                Map<INode<T>, ILink> neighbors = currentNode.getNeighbors();
                ILink link = neighbors.get(nextNode);

                irb.releaseSpecifiedBandwidth(src, dst, currentNode, nextNode, link, bw);
            }
        }
    }

    public void setFlowPusherService(IStaticFlowEntryPusherService flowPusherService) {
        irb.setFlowPusher(flowPusherService);
    }

    public boolean needFlowPusher() {
        return irb.needFlowPusher();
    }
}
