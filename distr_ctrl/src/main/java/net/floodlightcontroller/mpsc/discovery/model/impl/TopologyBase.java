package net.floodlightcontroller.mpsc.discovery.model.impl;


import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.internal.IOFSwitchService;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.learningswitch.ILearningSwitchService;
import net.floodlightcontroller.linkdiscovery.ILinkDiscovery;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;

import net.floodlightcontroller.linkdiscovery.LinkInfo;
import net.floodlightcontroller.mpsc.xml.GeneralConfigParser;
import net.floodlightcontroller.mpsc.xml.TopoConfigParser;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.ITopology;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.openflow.customservices.IHostHolderService;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.HostHolderImpl;
import net.floodlightcontroller.statistics.IStatisticsService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class TopologyBase implements ITopology {
    protected static Logger log = LoggerFactory.getLogger(ITopology.class);
    private ILinkDiscoveryService linkDiscoveryService;
    IOFSwitchService switchService;
    private IStaticFlowEntryPusherService flowPusher;
    private IStatisticsService statService;
    private IHostHolderService hostHolder;
    private Map<Object, INode> knownNodes;
    private Map<Object, INode> knownForeignNodes;
    private Map<Pair<INode, INode>, ILink> knownLinks;
    private Map<Pair<INode, INode>, ILink> knownForeignLinks;
    private FloodlightModuleContext context;

    public TopologyBase() {
        this.hostHolder = HostHolderImpl.getInstance();
        this.knownNodes = new HashMap<>();
        this.knownForeignNodes = new HashMap<>();
        this.knownLinks = new HashMap<>();
        this.knownForeignLinks = new HashMap<>();
    }

    public void setContext(FloodlightModuleContext context) {
        this.context = context;
        this.linkDiscoveryService = context.getServiceImpl(ILinkDiscoveryService.class);
        this.flowPusher = context.getServiceImpl(IStaticFlowEntryPusherService.class);
        this.switchService = context.getServiceImpl(IOFSwitchService.class);
        this.statService = context.getServiceImpl(IStatisticsService.class);
    }

    @Deprecated
    public TopologyBase(ILinkDiscoveryService linkDiscoveryService, IStaticFlowEntryPusherService flowPusher, IHostHolderService hostHolder) {
        this.linkDiscoveryService = linkDiscoveryService;
        this.flowPusher = flowPusher;
        this.hostHolder = hostHolder;
        this.knownNodes = new HashMap<>();
        this.knownForeignNodes = new HashMap<>();
        this.knownLinks = new HashMap<>();
        this.knownForeignLinks = new HashMap<>();
    }

    public void updateTopoCache() {
        //flush all cache
        knownNodes.clear();
        knownNodes.clear();
        knownLinks.clear();
        knownForeignLinks.clear();

        if (GeneralConfigParser.getInstance().DYNAMIC_GRAPH)
            TopoConfigParser.Update();

        //add all known by controller switches first
        Map<DatapathId, IOFSwitch> switches = switchService.getAllSwitchMap();
        for (DatapathId id : switches.keySet())
            System.out.println("[DEBUG-MPSC] Connected switch with id=" + id);
        for (DatapathId id : switches.keySet())
            knownNodes.put(id.toString(), new SwitchNodeBase(id.toString(), this));

        //add all known by controller switch-to-switch links
        Map<DatapathId, Set<Link>> switchLinks = linkDiscoveryService.getSwitchLinks();
        for (DatapathId id : switchLinks.keySet())
            for (Link l : switchLinks.get(id))
                System.out.println("[DEBUG-MPSC] Switch id=" + id + " has link src=" + l.getSrc() + " dst=" + l.getDst());
        for (Set<Link> links : switchLinks.values())
            for (Link l : links) {
                LinkInfo lInfo = linkDiscoveryService.getLinkInfo(l);
                if (lInfo.getLinkType() != ILinkDiscovery.LinkType.INVALID_LINK && (l.getDst() != null && l.getSrc() != null)) {
                    Pair<INode, INode> linkKey1 = new ImmutablePair<>(knownNodes.get(l.getSrc().toString()), knownNodes.get(l.getDst().toString()));
                    Pair<INode, INode> linkKey2 = new ImmutablePair<>(knownNodes.get(l.getDst().toString()), knownNodes.get(l.getSrc().toString()));
                    if (!knownLinks.containsKey(linkKey1) && !knownLinks.containsKey(linkKey2))
                        knownLinks.put(linkKey1, new SwitchLinkBase(this, l));
                }
            }

        //TODO add all know inter-controller switch links
        Map<DatapathId, Set<Link>> switchForeignLinks = linkDiscoveryService.getSwitchForeignLinks();
        for (Set<Link> links : switchForeignLinks.values())
            for (Link l : links) {
                if (l.getDst() != null && l.getSrc() != null) {
                    String src = l.getSrc().toString();
                    String dst = l.getDst().toString();
                    if (!knownNodes.containsKey(src) && !knownForeignNodes.containsKey(src))
                        knownForeignNodes.put(src, new SwitchNodeBase(src, this));
                    if (!knownNodes.containsKey(dst) && !knownForeignNodes.containsKey(dst))
                        knownForeignNodes.put(dst, new SwitchNodeBase(dst, this));
                    INode srcN = knownNodes.containsKey(src) ? knownNodes.get(src) : knownForeignNodes.get(src);
                    INode dstN = knownNodes.containsKey(dst) ? knownNodes.get(dst) : knownForeignNodes.get(dst);


                    Pair<INode, INode> linkKey1 = new ImmutablePair<>(srcN, dstN);
                    Pair<INode, INode> linkKey2 = new ImmutablePair<>(dstN, srcN);
                    if (!knownForeignLinks.containsKey(linkKey1) && !knownForeignLinks.containsKey(linkKey2))
                        knownForeignLinks.put(linkKey1, new SwitchLinkBase(this, l));
                }
            }
//        IHAWorkerService haworker = context.getServiceImpl(IHAWorkerService.class);
//        LDHAWorker ldhaworker = haworker.getService("LDHAWorker");

        //add all known by controller hosts
        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> hostSwAndPort : hostHolder.getHostsAndSwitchesToPortsMap().entrySet()) {
            String hostIP = hostSwAndPort.getKey().getLeft().toString();
            DatapathId switchDPID = hostSwAndPort.getKey().getRight();
            short port = hostSwAndPort.getValue();

            INode host = new HostNodeBase<>(hostIP, switchDPID.toString(), port, this);
            knownNodes.put(hostIP, host);
            Pair<INode, INode> key = new ImmutablePair<>(host, knownNodes.get(switchDPID.toString()));
            knownLinks.put(key, new HostLinkBase(this, hostIP, switchDPID, port));
        }

        System.out.println("[DEBUG-MPSC] Known nodes:");
        for (INode n : knownNodes.values())
            System.out.println("[DEBUG-MPSC] Node id=" + n.getID());

        System.out.println("[DEBUG-MPSC] Known foreign nodes:");
        for (INode n : knownForeignNodes.values())
            System.out.println("[DEBUG-MPSC] Foreign node id=" + n.getID());


        System.out.println("[DEBUG-MPSC] Known links:");
        for (Pair<INode, INode> l : knownLinks.keySet())
            System.out.println("[DEBUG-MPSC] Link src=" + l.getLeft().getID() + " dst=" + l.getRight().getID()
                    + " link=" + knownLinks.get(l));

        System.out.println("[DEBUG-MPSC] All known foreign links:");
        for (Pair<INode, INode> p : knownForeignLinks.keySet())
            System.out.println("[DEBUG-MPSC] link src=" + p.getLeft().getID() + " (is foreign="
                    + !knownNodes.keySet().contains(p.getLeft().getID())
                    + ") dst=" + p.getRight().getID() + " (is foreign="
                    + !knownNodes.keySet().contains(p.getRight().getID()) + ")");

        //init neighbors based on cache!
        initNeighbors();

        if (log.isInfoEnabled())
            log.info("[NM-Topology] Topology Cache has been successfully updated!");
    }

    private void initNeighbors() {
        for (INode n : knownNodes.values())
            n.clearNeighbors();

        for (INode n : knownForeignNodes.values())
            n.clearNeighbors();

        for (Map.Entry<Pair<INode, INode>, ILink> entry : knownLinks.entrySet()) {
            entry.getKey().getLeft().addNeighbor(entry.getKey().getRight(), entry.getValue());
            entry.getKey().getRight().addNeighbor(entry.getKey().getLeft(), entry.getValue());
        }

        for (Map.Entry<Pair<INode, INode>, ILink> entry : knownForeignLinks.entrySet()) {
            entry.getKey().getLeft().addNeighbor(entry.getKey().getRight(), entry.getValue());
            entry.getKey().getRight().addNeighbor(entry.getKey().getLeft(), entry.getValue());
        }
    }

    @Override
    public void prepareTopoForDijkstra(double bw, boolean isSPF) {
        for (INode n : knownNodes.values()) {
            n.setMinDist(Double.POSITIVE_INFINITY);
            n.setPredecessor(null);
            n.clearAllPredecessorsAndMinDists();
            n.clearDominantPaths();
        }

        for (ILink l : knownLinks.values())
            if (l.getBw() < bw)
                l.setAvCost(Double.POSITIVE_INFINITY);
            else if (isSPF)
                l.setAvCost(1);
            else
                l.setAvCost(l.getCost());
        /*
        //prepare nodes
        for (Node n : getNodesArray()) {
            n.nm_minDist.clear();
            n.nm_predecessor.clear();
            n.minDistance = Double.POSITIVE_INFINITY;
            n.previous = null;
            n.dstAheadVector[0] = Double.POSITIVE_INFINITY;
            n.dstAheadVector[1] = Double.POSITIVE_INFINITY;
            n.clearDominantPaths();
        }
        */
    }

    @Override
    public Collection<INode> getKnownNodes() {
        return knownNodes.values();
    }

    @Override
    public <T> INode<T> getNodeWithID(T id) {
        if (knownNodes.containsKey(id))
            return knownNodes.get(id);
        else if (knownForeignNodes.containsKey(id))
            return knownForeignNodes.get(id);
        else
            return null;
            /*{
            IPv4Address nodeIP = null;
            if (id instanceof IPv4Address)
                nodeIP = (IPv4Address) id;
            else if (id instanceof String)
                nodeIP = IPv4Address.of((String) id);

            if (nodeIP != null && hostHolder.getSwitchesAndPortsByHost(nodeIP) != null) {
                Set<Pair<DatapathId, Short>> switchesAndPorts = hostHolder.getSwitchesAndPortsByHost(nodeIP);//todo here is an assumption that host is connected only to one border switch!!!
                if (switchesAndPorts != null && !switchesAndPorts.isEmpty()) {
                    Pair<DatapathId, Short> switchAndPort = switchesAndPorts.iterator().next();

                    return new HostNodeBase<T>(id, (T) switchAndPort.getLeft().toString(),
                            switchAndPort.getRight(), this);
                }
            }
        }
        throw new UnsupportedOperationException("[NM-model]" + id.getClass() +
                " type of node ID currently isn't supported or "
                + id + " node is unknown for controller!"); */
    }

    public Map<Pair<INode, INode>, ILink> getKnownLinks() {
        return this.knownLinks;
    }

    @Override
    public Map<Pair<INode, INode>, ILink> getKnownForeignLinks() {
        return this.knownForeignLinks;
    }

    public Set<Pair<IPv4Address, Short>> getSwitchHostsAndPorts(DatapathId dpid) {
        return hostHolder.getAllHostsAndPortsBySwitch(dpid);
    }

    public ILinkDiscoveryService getLinkDiscoveryService() {
        return context.getServiceImpl(ILinkDiscoveryService.class);
    }

    public IStaticFlowEntryPusherService getFlowPusher() {
        return context.getServiceImpl(IStaticFlowEntryPusherService.class);
    }

    public IStatisticsService getStatService() {
        return context.getServiceImpl(IStatisticsService.class);
    }

    protected <T> boolean isNodeForeign(INode<T> node)
    {
        return knownForeignNodes.keySet().contains(node.getID());
    }
}
