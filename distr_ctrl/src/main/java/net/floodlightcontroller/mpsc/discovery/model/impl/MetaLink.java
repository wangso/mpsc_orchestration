package net.floodlightcontroller.mpsc.discovery.model.impl;

import net.floodlightcontroller.mpsc.discovery.model.INode;
import org.projectfloodlight.openflow.types.DatapathId;

/**
 * Created by dmitriichemodanov on 2/28/18.
 */
public class MetaLink extends LinkBaseAbstract {

    public MetaLink()
    {
        super(null);
    }

    @Override
    public double getBw() {
        return Double.MAX_VALUE;
    }

    @Override
    public double getLatency() {
        return 0.0;
    }

    @Override
    public <T> short getSrcPort(INode<T> src, INode<T> dst) {
        return 0;
    }

}
