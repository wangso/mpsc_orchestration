package net.floodlightcontroller.mpsc.discovery.model;

import net.floodlightcontroller.mpsc.MPSCConstants;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface ILink extends MPSCConstants
{
    public double getBw();

    public double getCost();

    public void setCost(double cost);

    public double getLatency();

    public <T> short getSrcPort(INode<T> src, INode<T> dst);

    public double getAvCost();

    public void setAvCost(double cost);
}
