package net.floodlightcontroller.mpsc.xml;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chemo_000 on 9/18/2015.
 */
public class TopoConfigParser {
    protected static Logger log = LoggerFactory.getLogger(TopoConfigParser.class);

    private Map<Pair<String, String>, Double> linkLatency;
    private Map<Pair<String, String>, Double> linkCapacity;
    private Map<String, Double> nodeCapacity;

    private static TopoConfigParser instance = new TopoConfigParser("topo-config.xml");

    public static TopoConfigParser getInstance() {
        return instance;
    }

    private TopoConfigParser(String url) {
        this.linkLatency = new HashMap<>();
        this.linkCapacity = new HashMap<>();
        this.nodeCapacity = new HashMap<>();

        File fXmlFile = new File(url);

        try {
            parseFile(new FileInputStream(fXmlFile));
        } catch (Exception ex) {
            log.error("[NM-XMLParser]During parsing xml file " + url + " following exception occurred: " + ex.getMessage(), ex);
        }

        if (log.isInfoEnabled())
            log.info("[NM-XMLParser] XML Parser successfully read topology configuration file!");
    }

    protected TopoConfigParser() {
        this.linkLatency = new HashMap<>();
        this.linkCapacity = new HashMap<>();
    }

    public static void Update() {
        instance = new TopoConfigParser("topo-config.xml");
    }

    protected void parseFile(InputStream fXmlStream) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlStream);
        doc.getDocumentElement().normalize();
        Element root = doc.getDocumentElement();

        NodeList links = root.getElementsByTagName("link");
        for (int i = 0; i < links.getLength(); i++) {
            NamedNodeMap attributes = links.item(i).getAttributes();
            Node src = attributes.getNamedItem("src");
            Node dst = attributes.getNamedItem("dst");
            if (src != null && dst != null) {
                Pair<String, String> key = new ImmutablePair<>(src.getTextContent(), dst.getTextContent());
                Node cost = attributes.getNamedItem("latency");
                if (cost != null)
                    try {
                        linkLatency.put(key, Double.valueOf(cost.getTextContent()));
                    } catch (Exception ex) {
                        //do nothing
                    }

                Node capacity = attributes.getNamedItem("capacity");
                if (capacity != null)
                    try {
                        linkCapacity.put(key, Double.valueOf(capacity.getTextContent()));
                    } catch (Exception ex) {
                        //do nothing
                    }
            } else continue;
        }

        NodeList nodes = root.getElementsByTagName("node");
        for (int i = 0; i < nodes.getLength(); i++) {
            NamedNodeMap attributes = nodes.item(i).getAttributes();
            Node id = attributes.getNamedItem("id");
            if (id != null) {
                Node capacity = attributes.getNamedItem("capacity");
                if (capacity != null)
                    try {
                        nodeCapacity.put(id.getTextContent(), Double.valueOf(capacity.getTextContent()));
                    } catch (Exception ex) {
                        //do nothing
                    }
            } else continue;
        }
    }

    public Double getLinkLatency(Pair<String, String> linkSrcDstKey) {
        return linkLatency.get(linkSrcDstKey);
    }

    public Double getLinkCapacity(Pair<String, String> linkSrcDstKey) {
        return linkCapacity.get(linkSrcDstKey);
    }

    public double getNodeCapacity(String id) {
        if (nodeCapacity.containsKey(id))
            return nodeCapacity.get(id);
        else
            return 0.0;
    }
}
