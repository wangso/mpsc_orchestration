package net.floodlightcontroller.mpsc.openflow.controller;

import java.util.*;

import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.restserver.IRestApiService;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.mpsc.discovery.model.ITopology;
import net.floodlightcontroller.mpsc.discovery.model.impl.TopologyBase;
import net.floodlightcontroller.mpsc.openflow.customservices.IHostHolderService;
import net.floodlightcontroller.mpsc.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.HostHolderImpl;
import net.floodlightcontroller.mpsc.mapping.routing.RouteFinder;

import net.floodlightcontroller.mpsc.web.MPSCWebRoutable;
import net.floodlightcontroller.mpsc.xml.GeneralConfigParser;
import net.floodlightcontroller.mpsc.xml.TopoConfigParser;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.openflow.protocol.OFMatch;
import org.projectfloodlight.openflow.protocol.*;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.OFPort;
import org.restlet.data.MediaType;
import org.restlet.resource.ClientResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MPSCControllerV01 implements IOFMessageListener, IFloodlightModule {
    protected IFloodlightProviderService floodlightProvider;
    private ILinkDiscoveryService linkDiscoveryService;
    private IHostHolderService hostHolder;
    protected IRestApiService restApi;
    private ITopology topo = new TopologyBase();
//    private IStaticFlowEntryPusherService flowPusher;

    protected static Logger log = LoggerFactory.getLogger(MPSCControllerV01.class);

    @Override
    public String getName() {
        return MPSCControllerV01.class.getSimpleName();
    }

    @Override
    public boolean isCallbackOrderingPrereq(OFType type, String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isCallbackOrderingPostreq(OFType type, String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public net.floodlightcontroller.core.IListener.Command receive(IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {
//        if (msg.getType() != OFType.PACKET_IN)
//        {
//            // Allow the next module to also process this OpenFlow message
//            return Command.CONTINUE;
//        }

        OFPacketIn pi = (OFPacketIn) msg;


        //Parse the received packet
        OFMatch match = new OFMatch();
        match.loadFromPacket(pi.getData(), pi.getInPort().getShortPortNumber());

        //handle all IP/ARP packets
        if (match.getDataLayerType() == Ethernet.TYPE_IPv4 || match.getDataLayerType() == Ethernet.TYPE_ARP) {
            IPv4Address srcIP = IPv4Address.of(match.getNetworkSource());
            IPv4Address dstIP = IPv4Address.of(match.getNetworkDestination());
            if (!hostHolder.containsIPAndDPID(srcIP, sw.getId()) &&
                    isHostDirectlyConnected(sw.getId(), pi.getInPort().getShortPortNumber())) {
                hostHolder.put(new ImmutablePair<>(srcIP, sw.getId()),
                        pi.getInPort().getShortPortNumber());

                if (log.isDebugEnabled())
                    log.debug("[NM-Controller]New host with src IP=" + srcIP + " dst IP=" + dstIP
                            + " has been seen on switch with DPID=" + sw.getId()
                            + " port=" + pi.getInPort().toString());
            }

            log.info("[NM-Controller]Can't forward UNKNOWN for OFSwitch ARP OR IPV4 Packet with src IP="
                    + srcIP + " dst IP=" + dstIP + " on switch=" + sw.getId());

            return Command.STOP;
        }
        //return Command.STOP;
        return Command.CONTINUE;
    }

    private boolean isHostDirectlyConnected(DatapathId dpid, short port) {
        Set<Link> switchLinks = linkDiscoveryService.getSwitchLinks().get(dpid);

        if (log.isTraceEnabled())
            log.trace("[MPSC-Controller]Switch dpid=" + dpid + " has next links: " + switchLinks);

        if (switchLinks != null && !switchLinks.isEmpty())//todo if switch doesn't have links it's uncertainty! Two possibilities currently controller lose information about switch neighbors or
        {
            for (Link link : switchLinks) {
                OFPort switchPort = link.getSrc().equals(dpid) ? link.getSrcPort() : link.getDstPort();
                //if packet came to same port on which switch has neighbor switch, host ISN'T directly connected to this switch!
                if (switchPort.getShortPortNumber() == port)
                    return false;
            }

            return true;
        } else {
            if (log.isDebugEnabled())
                log.debug("[MPSC-Controller]Uncertainty has been detected: no neighbors for switch id=" + dpid);

            return true;
        }
    }

    @Override
    public Collection<Class<? extends IFloodlightService>> getModuleServices() {
        Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
//        l.add(IStaticFlowEntryPusherService.class);
        l.add(IRouteFinderService.class);
        return l;
    }

    @Override
    public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
        Map<Class<? extends IFloodlightService>, IFloodlightService> m = new HashMap<Class<? extends IFloodlightService>, IFloodlightService>();
        m.put(IRouteFinderService.class, new RouteFinder(topo));
        return m;
    }

    @Override
    public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
        Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
        l.add(IFloodlightProviderService.class);
        l.add(IRestApiService.class);
        return l;
    }

    @Override
    public void init(FloodlightModuleContext context)
            throws FloodlightModuleException {
        floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
        linkDiscoveryService = context.getServiceImpl(ILinkDiscoveryService.class);

        hostHolder = HostHolderImpl.getInstance();

        restApi = context.getServiceImpl(IRestApiService.class);

        topo.setContext(context);

        if (log.isInfoEnabled())
            log.info("[MPSC-Module] XML Parser has been started.");
        GeneralConfigParser.getInstance();
        TopoConfigParser.getInstance();

        //TODO: make periodical calls to cntrl to overcome potential connectivity issues
        ClientResource resource = new ClientResource("http://" + GeneralConfigParser.getInstance().CORE_CTRL_IP
                + ":2223/SDN/register");
        try {
            resource.get();
            if (log.isInfoEnabled())
                log.info("[MPSC-Module] Registered at the centralized controller.");
        } catch (Exception ex) {
            if (log.isInfoEnabled())
                log.info("[MPSC-Module] Failed to register at the centralized controller.");
        }

        /*
        try
        {
            //old way to send requests
            //init and launch in different thread Virtual Link Requests Handler
            new Thread(new VLRequestHandlerImpl(context)).start(); //todo take Flow Pusher from instance
        } catch (Throwable t)
        {
            log.error("[MPSC-Module]Next exception has occurred: ", t);
        }
        */
    }

    @Override
    public void startUp(FloodlightModuleContext context) {
        floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
        restApi.addRestletRoutable(new MPSCWebRoutable());
    }
}