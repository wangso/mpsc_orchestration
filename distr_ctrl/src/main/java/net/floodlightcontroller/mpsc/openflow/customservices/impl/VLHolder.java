package net.floodlightcontroller.mpsc.openflow.customservices.impl;

import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.web.model.VirtualLink;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VLHolder <T>
{
    private static VLHolder instance = new VLHolder();

    private Map<String, List<VirtualLink>> userVL;

    private Map<VirtualLink, List<INode<String>>> vlToPath;

    public static VLHolder getInstance()
    {
        return instance;
    }

    private VLHolder()
    {
        userVL = new ConcurrentHashMap<>();
        vlToPath = new ConcurrentHashMap<>();
    }

    public List<VirtualLink> getUserVL(String userName)
    {
        return userVL.get(userName);
    }

    public void addUserVL(String userName, List<VirtualLink> vls)
    {
        if(userVL.containsKey(userName))

            userVL.get(userName).addAll(vls);
        else
            userVL.put(userName, vls);
    }

    public void addPartialPath(VirtualLink vl, List<INode<String>> partialPath)
    {
        vlToPath.remove(vl);
        vlToPath.put(vl, partialPath);
    }

    public void removeVL(VirtualLink vl)
    {
        vlToPath.remove(vl);
    }

    public List<INode<String>> getPartialPath(VirtualLink vl)
    {
        return vlToPath.get(vl);
    }

    public void removeAllVLs()
    {
        userVL.clear();
        vlToPath.clear();
    }
}
