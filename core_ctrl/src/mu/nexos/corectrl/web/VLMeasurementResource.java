package mu.nexos.corectrl.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mu.nexos.corectrl.distrctrl.DistrCtrlHolder;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import mu.nexos.corectrl.web.model.VirtualLink;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;
import java.util.*;

import static mu.nexos.corectrl.web.SwitchClientResource.measureVLs;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class VLMeasurementResource extends ServerResource implements IJsonVL {

    @Post("json")
    public String measureVL(String json) {
        String user = getAttribute("user");
        if (user != null)
            try {
                //for each VL measure its available bandwidth assuming no traffic on the VL itself!
                List<VirtualLink> vls = jsonToVL(json);
                double[][] avBw = new double[DistrCtrlHolder.instance.getSubscribers().size()][vls.size()];
                int i = 0;
                // TODO: optimize distr cntrls calls - verify that dist cntrl is participating in VL maintenance
                // TODO: and make asynchronous calls to it
                for (DistrCtrlKey key : DistrCtrlHolder.instance.getSubscribers()) {
                    double[] partialVlAvBw = measureVLs(key, vls);
                    System.out.println("[MPSC-Measurement] Next avBw values from cntrl[" +
                            key + "] are obtained:" + Arrays.toString(partialVlAvBw));
                    avBw[i] = partialVlAvBw;
                    i++;
                }
                return arrayToJson(computeVLAvBw(avBw));
            } catch (Exception ex) {
                ex.printStackTrace();
                return ("{\"ERROR\" : \"exception during VL measurement: " + ex.getMessage() + "\"}");
            }
        else
            return ("{\"ERROR\" : \"user=null\"}");
    }

    private double[] computeVLAvBw(double[][] avBw) {
        double[] avBwVls = new double[avBw[0].length];
        for (int i = 0; i < avBw[0].length; i++) {
            double[] vlCntrlBw = new double[avBw.length];
            for(int j = 0; j < avBw.length; j++)
                vlCntrlBw[j] = avBw[j][i];
            avBwVls[i] = arrayMin(vlCntrlBw);
        }
        return avBwVls;
    }

    private double arrayMin(double[] d) {
        double max = Double.MAX_VALUE;
        for (double dd : d)
            if (dd < max)
                max = dd;
        return max;
    }

    public static List<VirtualLink> jsonToVL(String json) throws IOException {
        System.out.println("Next JSON obtained: " + json);
        ObjectMapper mapper = new ObjectMapper();
        //deserialize JSON objects as a Set
        List<VirtualLink> vls = mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(List.class, VirtualLink.class));
        return vls;
    }

    public String getAttribute(String name) {
        Object value = this.getRequestAttributes().get(name);
        return value == null ? null : value.toString();
    }

    public static String arrayToJson(double[] d) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(d);
        return json;
    }
}
