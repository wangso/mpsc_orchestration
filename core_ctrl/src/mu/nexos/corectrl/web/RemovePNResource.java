package mu.nexos.corectrl.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import mu.nexos.corectrl.web.model.SwitchRule;
import mu.nexos.corectrl.web.model.VLRules;
import mu.nexos.corectrl.web.model.VirtualLink;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by dmitriichemodanov on 9/28/18.
 */
public class RemovePNResource {
    public static void remove(DistrCtrlKey key, Map<VirtualLink, List<SwitchRule>> newRules) {

        //the path should be the same for all distributed SDN cntrls
        String URI = "http://" + key.getIP() + ":" + key.getPort() + "/wm/mpsc/Remove_PN/admin";
        System.out.println("[DEBUG] Switch URI=" + URI);
        ClientResource resource = new ClientResource(URI);

        try {
            List<VLRules> vlRulesList = new ArrayList<>(newRules.keySet().size());
            for (VirtualLink vl : newRules.keySet())
                vlRulesList.add(new VLRules(vl, switchRuleToJson(newRules.get(vl))));

            String requestJSON = vlRulesToJson(vlRulesList);
            System.out.println("[DEBUG] Json request=" + requestJSON);
            Representation req = new StringRepresentation(requestJSON, MediaType.APPLICATION_JSON);
            resource.post(req);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static String switchRuleToJson(List<SwitchRule> switchRules) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(switchRules);

        return json;
    }


    private static String vlRulesToJson(List<VLRules> vlRulesList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vlRulesList);

        return json;
    }
}
