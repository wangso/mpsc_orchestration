package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import mu.nexos.corectrl.model.INode;


/**
 * Created by chemo_000 on 4/29/2015.
 */
@JsonSerialize(using = PhysicalLinkSerializer.class)
@JsonDeserialize(using = PhysicalLinkDeserializer.class)
public class PhysicalLink
{
    protected String src;
    protected INode.INodeType srcType;
    protected double srcCap;
    protected double srcStress;
    protected String dst;
    protected INode.INodeType dstType;
    protected double dstCap;
    protected double dstStress;
    protected double avBw = 0;
    protected double capacity = 0;
    protected double latency = 0;

    public PhysicalLink()
    {
    }

    public PhysicalLink(String src, INode.INodeType srcType, String dst,
                        INode.INodeType dstType, double avBw, double capacity, double latency) {
        this(src, srcType, 0, 0, dst, dstType, 0, 0, avBw, capacity, latency);
    }
    public PhysicalLink(String src, INode.INodeType srcType, double srcCap, double srcStress,
                        String dst, INode.INodeType dstType, double dstCap, double dstStress,
                        double avBw, double capacity, double latency) {
        this.src=src;
        this.srcType=srcType;
        this.srcCap = srcCap;
        this.srcStress = srcStress;
        this.dst=dst;
        this.dstType=dstType;
        this.dstCap=dstCap;
        this.dstStress=dstStress;
        this.avBw=avBw;
        this.capacity=capacity;
        this.latency =latency;
    }

    public String toString() {
        return "src=" + src + ",sType=" + srcType + ",dst=" + dst + ",dType=" + dstType + ",avBw=" + avBw
                + ",cap=" + capacity + ",lat=" + latency;
    }

    public String getSrc()
    {return this.src;}

    public String getDst()
    {return this.dst;}

    public INode.INodeType getSrcType()
    {
        return this.srcType;
    }

    public INode.INodeType getDstType()
    {
        return this.dstType;
    }

    public double getSrcCap()
    {
        return this.srcCap;
    }

    public double getDstCap()
    {
        return this.dstCap;
    }

    public  double getSrcStress()
    {
        return this.srcStress;
    }

    public double getDstStress()
    {
        return this.dstStress;
    }

    public double getAvBw()
    {
        return this.avBw;
    }

    public double getCapacity()
    {
        return this.capacity;
    }

    public double getLatency()
    {
        return this.latency;
    }
}
