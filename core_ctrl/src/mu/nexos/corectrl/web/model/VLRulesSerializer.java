package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import mu.nexos.corectrl.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VLRulesSerializer extends JsonSerializer<VLRules> implements IJsonVL
{
    @Override
    public void serialize(VLRules vlR, JsonGenerator jgen, SerializerProvider arg) throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeStringField(SRC, vlR.getVL().src);
        jgen.writeStringField(SRC_IP, vlR.getVL().srcIP);
        jgen.writeNumberField(SRC_DEMAND, vlR.getVL().src_cap);
        jgen.writeStringField(DST, vlR.getVL().dst);
        jgen.writeStringField(DST_IP, vlR.getVL().dstIP);
        jgen.writeNumberField(DST_DEMAND, vlR.getVL().dst_cap);
        jgen.writeNumberField(BW, vlR.getVL().bw);
        jgen.writeNumberField(LATENCY, vlR.getVL().latency);
        jgen.writeStringField(STATUS, vlR.getVL().status.toString());
        jgen.writeStringField(ERROR, vlR.getVL().error);
        jgen.writeStringField(SWITCH_RULES, vlR.switchRules);
        //jgen.writeStringField(MP_CANDIDATES, vlR.mpCandidates);
        jgen.writeEndObject();
    }
}
