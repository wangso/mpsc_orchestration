package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.web.IJsonVL;

import java.io.IOException;
import java.util.List;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class MPathSerializer extends JsonSerializer<MPath> implements IJsonVL
{
    @Override
    public void serialize(MPath mp, JsonGenerator jgen, SerializerProvider arg) throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeStringField(SRC, mp.src);
        jgen.writeStringField(DST, mp.dst);
        jgen.writeStringField(PATH, pathToJson(mp.path));
        jgen.writeNumberField(COST, mp.cost);
        jgen.writeEndObject();
    }

    public static String pathToJson(List<INode<String>> path) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(path);

        return json;
    }
}
