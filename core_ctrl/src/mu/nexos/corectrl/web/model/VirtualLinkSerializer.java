package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import mu.nexos.corectrl.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VirtualLinkSerializer extends JsonSerializer<VirtualLink> implements IJsonVL
{
    @Override
    public void serialize(VirtualLink vl, JsonGenerator jgen, SerializerProvider arg) throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeStringField(SRC, vl.src);
        jgen.writeStringField(SRC_IP, vl.srcIP);
        jgen.writeNumberField(SRC_DEMAND, vl.src_cap);
        jgen.writeStringField(DST, vl.dst);
        jgen.writeStringField(DST_IP, vl.dstIP);
        jgen.writeNumberField(DST_DEMAND, vl.dst_cap);
        jgen.writeNumberField(BW, vl.bw);
        jgen.writeNumberField(LATENCY, vl.latency);
        jgen.writeStringField(STATUS, vl.status.toString());
        jgen.writeStringField(ERROR, vl.error);
        jgen.writeEndObject();
    }
}
