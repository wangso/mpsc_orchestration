package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by dmitriichemodanov on 9/28/18.
 */
@JsonSerialize(using = VLRulesSerializer.class)
@JsonDeserialize(using = VLRulesDeserializer.class)
public class VLRules {
    protected VirtualLink vl;

    protected String switchRules;

    protected String mpCandidates;

    public VLRules(VirtualLink vl, String switchRules)
    {
        this.vl = vl;
        this.switchRules = switchRules;
    }

    public VLRules(VirtualLink vl, String switchRules, String mpCandidates)
    {
        this(vl, switchRules);
        this.mpCandidates = mpCandidates;
        //set MP candidates to VL
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<MPath> mpaths = mapper.readValue(this.mpCandidates,
                    mapper.getTypeFactory().constructCollectionType(List.class, MPath.class));
            this.vl.setMpCandidates(mpaths);
        } catch (IOException ex) {
            System.out.println("[ERROR-MPSC] Cannot parse mp candidates=" + this.mpCandidates
                    + "; reason:" + ex.getMessage());
        }
    }

    public VirtualLink getVL() {
        return vl;
    }

    public List<SwitchRule> getSwitchRules() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<SwitchRule> rules = mapper.readValue(getSwitchRulesAsJSON(),
                    mapper.getTypeFactory().constructCollectionType(List.class, SwitchRule.class));
            return rules;
        } catch (IOException ex) {
            System.out.println("[ERROR-MPSC] Cannot parse switch rules=" + this.switchRules
                    + "; reason:" + ex.getMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public String getSwitchRulesAsJSON()
    {
        return this.switchRules;
    }

    public String getMpCandidatesAsJSON()
    {
        return this.mpCandidates;
    }

    public void setMpCandidatesJSON(String json)
    {
        this.mpCandidates = json;
    }
}
