package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import mu.nexos.corectrl.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VLRulesDeserializer extends JsonDeserializer<VLRules> implements IJsonVL {

    @Override
    public VLRules deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException {
        String src = null;
        String srcIP = "-";
        String dst = null;
        String dstIP = "-";
        double src_demand = 0;
        double dst_demand = 0;
        double bw = 0;
        double latency = 0;
        VirtualLink.Status status = null;
        String error = "";
        String switchRules = null;
        String mpCandidates = null;

        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME) {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals("")) {
                continue;
            }

            switch (n) {
                case SRC:
                    src = jp.getText();
                    break;
                case SRC_IP:
                    srcIP = jp.getText();
                    break;
                case SRC_DEMAND:
                    src_demand = jp.getDoubleValue();
                    break;
                case DST:
                    dst = jp.getText();
                    break;
                case DST_IP:
                    dstIP = jp.getText();
                    break;
                case DST_DEMAND:
                    dst_demand = jp.getDoubleValue();
                    break;
                case BW:
                    bw = jp.getDoubleValue();
                    break;
                case LATENCY:
                    latency = jp.getDoubleValue();
                    break;
                case STATUS:
                    status = VirtualLink.Status.valueOf(jp.getText());
                    break;
                case ERROR:
                    error = jp.getText();
                    break;
                case SWITCH_RULES:
                    switchRules = jp.getText();
                    break;
//                case MP_CANDIDATES:
//                    mpCandidates = jp.getText();
//                    break;
                default:
                    break;
            }
        }

        if (src != null && dst != null && bw >= 0 && latency >= 0 && status != null && switchRules != null) {
            VirtualLink vl = new VirtualLink(src, srcIP, src_demand, dst, dstIP, dst_demand, bw,
                    latency == 0 ? Double.MAX_VALUE : latency, status);
            System.out.println("[DEBUG-MPSC-VLRulesDeserializer] The following VL with src IP=" + vl.getSrcIP()
                    + " and dst IP=" + vl.getDstIP() + " has been retrieved... Found src IP="
                    + srcIP + " and dst IP=" + dstIP);
            vl.setError(error);
            if (mpCandidates != null)
                return new VLRules(vl, switchRules, mpCandidates);
            else
                return new VLRules(vl, switchRules);
        } else throw new IOException("wrong VL Rules parameters");
    }
}
