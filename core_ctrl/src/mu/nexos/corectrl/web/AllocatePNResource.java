package mu.nexos.corectrl.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import mu.nexos.corectrl.web.model.MPath;
import mu.nexos.corectrl.web.model.SwitchRule;
import mu.nexos.corectrl.web.model.VLRules;
import mu.nexos.corectrl.web.model.VirtualLink;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by dmitriichemodanov on 9/28/18.
 */
public class AllocatePNResource {
    public static List<VirtualLink> allocate(DistrCtrlKey key, Map<VirtualLink, List<SwitchRule>> newRules) {

        //the path should be the same for all distributed SDN cntrls
        String URI = "http://" + key.getIP() + ":" + key.getPort() + "/wm/mpsc/Allocate_PN/admin";
        System.out.println("[DEBUG] Switch URI=" + URI);
        ClientResource resource = new ClientResource(URI);

        try {
            List<VLRules> vlRulesList = new ArrayList<>(newRules.keySet().size());
            for (VirtualLink vl : newRules.keySet())
                vlRulesList.add(new VLRules(vl, switchRuleToJson(newRules.get(vl)),
                        mpCandidatesToJson(vl.getMpCandidates())));

            String requestJSON = vlRulesToJson(vlRulesList);
            System.out.println("[DEBUG] Json request=" + requestJSON);
            Representation req = new StringRepresentation(requestJSON, MediaType.APPLICATION_JSON);
            String str = resource.post(req).getText();//requestJSON, MediaType.APPLICATION_JSON).getText();
            System.out.println("[DEBUG-MPSC] Json reply=" + str);
            ObjectMapper mapper = new ObjectMapper();

            //deserialize JSON objects as a Set
            List<VirtualLink> vls = mapper.readValue(str,
                    mapper.getTypeFactory().constructCollectionType(List.class, VirtualLink.class));

            return vls;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Collections.EMPTY_LIST;
    }

    private static String switchRuleToJson(List<SwitchRule> switchRules) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(switchRules);

        return json;
    }

    private static String mpCandidatesToJson(List<MPath> mPaths) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(mPaths);

        return json;
    }

    private static String vlRulesToJson(List<VLRules> vlRulesList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vlRulesList);

        return json;
    }
}
