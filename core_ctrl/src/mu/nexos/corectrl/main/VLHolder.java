package mu.nexos.corectrl.main;

import mu.nexos.corectrl.web.model.VirtualLink;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VLHolder
{
    private static VLHolder instance = new VLHolder();

    private Map<String, List<VirtualLink>> userVL;

    public static VLHolder getInstance()
    {
        return instance;
    }

    private VLHolder()
    {
        userVL = new ConcurrentHashMap<>();
    }

    public List<VirtualLink> getUserVL(String userName)
    {
        return userVL.get(userName);
    }

    public void addUserVL(String userName, List<VirtualLink> vls)
    {
        if(userVL.containsKey(userName))

            userVL.get(userName).addAll(vls);
        else
            userVL.put(userName, vls);
    }

    public void removeAllVLs()
    {
        userVL.clear();
    }
}
