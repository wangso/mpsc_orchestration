package mu.nexos.corectrl.model.impl;


import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.INode;

import java.util.Map;

/**
 * Created by dmitriichemodanov on 2/28/18.
 */
public class MetaNode<T> extends NodeBaseAbstract<T> {

    private double capacity;

    public MetaNode(T id, double capacity)
    {
        super(id);

        this.capacity = capacity;
    }

    @Override
    public double getStress() {
        return this.capacity; //stress of metanodes is always equal to capacity
    }

    @Override
    public void setStress(double stress) {
    }

    @Override
    public INode.INodeType getType() {
        return INode.INodeType.META_NODE;
    }

    @Override
    public Map<INode, ILink> getNeighbors() {
        return neighbors;
    }
}
